﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(DanoneRewards.Startup))]
namespace DanoneRewards
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
