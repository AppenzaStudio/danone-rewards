﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace DanoneRewards.Utilities
{
    public static class UtilityHelper
    {
        public static void ExportToExcel(IList source, HttpResponseBase Response, string FileName)
        {
            var grid = new GridView();
            grid.DataSource = source;
            grid.DataBind();
            Response.ClearContent();
            Response.Buffer = true;
            Response.AddHeader("content-disposition", "attachment; filename=" + FileName + ".xls");
            Response.ContentType = "application/ms-excel";
            Response.ContentEncoding = System.Text.Encoding.Unicode;
            Response.BinaryWrite(System.Text.Encoding.Unicode.GetPreamble());
            Response.Charset = "";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            grid.RenderControl(htw);

            Response.Output.Write(sw.ToString());
            Response.Flush();
            Response.End();
        }

        public static void SendMail(string recipient, string subject, string message)
        {
            SmtpClient SmtpServer = new SmtpClient(ConfigurationManager.AppSettings["DanoneSmtpServer"], int.Parse(ConfigurationManager.AppSettings["DanoneSmtpPort"]));
            SmtpServer.UseDefaultCredentials = false;
            SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
            SmtpServer.EnableSsl = true;
            SmtpServer.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["DanoneMail"], ConfigurationManager.AppSettings["DanonePassword"]);

            MailMessage Mail = new MailMessage();
            Mail.From = new MailAddress(ConfigurationManager.AppSettings["DanoneSmtpFrom"]);
            Mail.To.Add(recipient);
            Mail.IsBodyHtml = true;
            Mail.Subject = subject;
            Mail.Body = message;
            SmtpServer.Send(Mail);
        }

        public static void SendRedeemedVoucherMail(string Recipient, string VoucherCode, string VoucherDate)
        {
            try
            {
                SmtpClient SmtpServer = new SmtpClient(ConfigurationManager.AppSettings["DanoneSmtpServer"], int.Parse(ConfigurationManager.AppSettings["DanoneSmtpPort"]));
                SmtpServer.UseDefaultCredentials = false;
                SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                SmtpServer.EnableSsl = true;
                SmtpServer.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["DanoneMail"], ConfigurationManager.AppSettings["DanonePassword"]);

                MailMessage Mail = new MailMessage();
                Mail.From = new MailAddress(ConfigurationManager.AppSettings["DanoneSmtpFrom"]);
                Mail.To.Add(Recipient);
                Mail.IsBodyHtml = true;
                Mail.Subject = ConfigurationManager.AppSettings["DanoneVoucherSubject"];
                Mail.Body = ReadHTMLTemplate(VoucherCode, VoucherDate, "template.html");
                SmtpServer.Send(Mail);
            }
            catch (Exception ex)
            {
                ex = ex;
            }
        }

        public static void SendStoreRedeemedVoucherMail(string StoreName, string VoucherCode, string VoucherDate)
        {
            try
            {
                SmtpClient SmtpServer = new SmtpClient(ConfigurationManager.AppSettings["DanoneSmtpServer"], int.Parse(ConfigurationManager.AppSettings["DanoneSmtpPort"]));
                SmtpServer.UseDefaultCredentials = false;
                SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                SmtpServer.EnableSsl = true;
                SmtpServer.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["DanoneMail"], ConfigurationManager.AppSettings["DanonePassword"]);

                var DanoneRecievers = ConfigurationManager.AppSettings["DanoneRecievers"].Split(new char[] { ',' });
                var MessageBody = ReadHTMLTemplate(VoucherCode, VoucherDate, "storetemplate.html")
                    .Replace("STORECODE", StoreName).Replace("REDEEMDATE", VoucherDate).Replace("VOUCHERCODE", VoucherCode);

                MailMessage Mail = new MailMessage();
                Mail.From = new MailAddress(ConfigurationManager.AppSettings["DanoneSmtpFrom"]);
                if (StoreName.ToLower().StartsWith("seif"))
                {
                    Mail.To.Add(ConfigurationManager.AppSettings["SeifStoreMail"]);
                    MessageBody = MessageBody.Replace("STORENAME", "Seif");
                }
                else
                {
                    Mail.To.Add(ConfigurationManager.AppSettings["EzabyStoreMail"]);
                    MessageBody = MessageBody.Replace("STORENAME", "Ezaby");
                }
                foreach (var item in DanoneRecievers)
                    Mail.To.Add(item);

                Mail.IsBodyHtml = true;
                Mail.Subject = ConfigurationManager.AppSettings["DanoneStoreRedeemSubject"];
                Mail.Body = MessageBody;
                SmtpServer.Send(Mail);
            }
            catch (Exception ex)
            {
                ex = ex;
            }
        }

        private static string ReadHTMLTemplate(string VoucherCode, string VoucherDate, string FileName)
        {
            var strTemplate = "";
            var fs = new System.IO.FileStream(HttpContext.Current.Server.MapPath("~/Content/HTML/" + FileName), System.IO.FileMode.Open, System.IO.FileAccess.Read, System.IO.FileShare.ReadWrite);
            var sr = new System.IO.StreamReader(fs);
            try
            {
                strTemplate = sr.ReadToEnd();
                strTemplate = strTemplate.Replace("xxxxx", VoucherCode).Replace("xtimestampx", VoucherDate);
            }
            catch (Exception ex)
            {
                ex = ex;
            }
            finally
            {
                sr.Close();
                fs.Close();
            }

            return strTemplate;
        }

        public static string AddCouponDashes(string CouponCode)
        {
            if (CouponCode.Length == 16 && !CouponCode.Contains('-'))
            {
                //4921-6597-8835-5256
                CouponCode = CouponCode.Substring(0, 4) + "-" + CouponCode.Substring(4, 4) + "-" + CouponCode.Substring(8, 4) + "-" + CouponCode.Substring(12, 4);
            }
            return CouponCode;
        }
    }
}