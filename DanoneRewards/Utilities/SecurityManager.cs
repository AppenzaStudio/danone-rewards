﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DanoneRewards.Utilities
{
    public static class SecurityManager
    {
        public static string GetMD5Password(string Password)
        {
            var Crypto = new System.Security.Cryptography.MD5CryptoServiceProvider();
            byte[] bs = System.Text.Encoding.UTF8.GetBytes(Password);
            bs = Crypto.ComputeHash(bs);
            var Builder = new System.Text.StringBuilder();
            foreach (byte b in bs)
            {
                Builder.Append(b.ToString("x2").ToLower());
            }
            return Builder.ToString();
        }
    }
}