﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace DanoneRewards
{
    public class RouteConfig
    {
        public static void RegisterRoutes(RouteCollection routes)
        {
            routes.IgnoreRoute("{resource}.axd/{*pathInfo}");

            routes.MapRoute(
                name: "AdminRoute",
                url: "AdminLink",
                defaults: new { culture = "en", controller = "Users", action = "AdminLoginPage", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "StoreRoute",
                url: "StoreLink",
                defaults: new { culture = "en", controller = "Stores", action = "SaveStore", id = UrlParameter.Optional }
            );

            routes.MapRoute(
                name: "Default",
                url: "{culture}/{controller}/{action}/{id}",
                defaults: new { culture = CultureHelper.GetDefaultCulture(), controller = "Home", action = "Index", id = UrlParameter.Optional }
            );
        }
    }
}
