﻿using DanoneRewards.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DanoneRewards.ViewModels
{
    public class RegisteredConsumers
    {
        public Consumer ConsumerModel { get; set; }
        public int CouponsNum { get; set; }

    }
}