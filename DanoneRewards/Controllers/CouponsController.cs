﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DanoneRewards.Models;
using Microsoft.AspNet.Identity;
using System.Collections;
using PagedList;

namespace DanoneRewards.Controllers
{

    public class CouponsController : BaseController
    {
        private DanoneRewardsEntities db = new DanoneRewardsEntities();

        // GET: Coupons
        [CustomAuthorize(Roles = "AdminUser")]
        public ActionResult Index()
        {
            var coupons = db.Coupons.Include(c => c.Consumer);
            return View(coupons.ToList().Take(10));
        }
        
        [CustomAuthorize(Roles = "AdminUser")]
        public ActionResult RedeemedCoupons(string SortOrder, string RedeemedBy, string RedeemDate, string currentFilter, string searchString, string RedeemedFilter, int? page)
        {
            var CouponsLst = from s in db.Coupons.Where(x => x.RedeemedBy != null) select s;
            CouponsLst = CouponsLst.Include(s => s.Consumer).Include(s => s.Store);
            var NumberOfCoupons = CouponsLst.Count();
            TempData["NumberOfCoupons"] = NumberOfCoupons;
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;

            RedeemedFilter = string.IsNullOrEmpty(RedeemedFilter) ? TempData["RedeemedDropdownFilter"] == null ? "All" : TempData["RedeemedDropdownFilter"].ToString() : RedeemedFilter;                                  
            if (RedeemedFilter == "Redeemed")
            {
                CouponsLst = CouponsLst.Where(s => s.RedeemStoreId != null);
            }
            else if (RedeemedFilter == "NotRedeemed")
            {
                CouponsLst = CouponsLst.Where(s => s.RedeemStoreId == null);
            }
            else
            {
                CouponsLst = CouponsLst.Include(s => s.Consumer);
            }

            TempData["RedeemedDropdownFilter"] = RedeemedFilter;


            if (string.IsNullOrEmpty(RedeemedBy))
            {
                RedeemedBy = "";
            }
            else
            {
                RedeemedBy = string.IsNullOrEmpty(RedeemedBy) ? TempData["RedeemedByFilter"] == null ? "" : TempData["RedeemedByFilter"].ToString() : RedeemedBy;
                CouponsLst = CouponsLst.Where(s => s.Consumer.Name.Contains(RedeemedBy));
            }
            TempData["RedeemedByFilter"] = RedeemedBy;

            if (string.IsNullOrEmpty(SortOrder) && TempData.Peek("RedeemedSortOrder") == null)
            {
                SortOrder = "Name";
                ViewBag.NameSortParm = "Name";
                ViewBag.VoucherCodeSortParm = "VoucherCode";
                ViewBag.RedeemedStoreDateSortParm = "RedeemedStoreDate";
                ViewBag.StoreNameSortParm = "StoreName";
                ViewBag.RedeemDateSortParm = "RedeemDate";
            }
            else if (string.IsNullOrEmpty(SortOrder) && TempData.Peek("RedeemedSortOrder") != null)
            {
                SortOrder = TempData.Peek("RedeemedSortOrder").ToString();
                ViewBag.NameSortParm = "Name";
                ViewBag.VoucherCodeSortParm = "VoucherCode";
                ViewBag.RedeemedStoreDateSortParm = "RedeemedStoreDate";
                ViewBag.StoreNameSortParm = "StoreName";
                ViewBag.RedeemDateSortParm = "RedeemDate";
            }
            else
            {
                ViewBag.NameSortParm = SortOrder == "Name" ? "Name_desc" : "Name";
                ViewBag.VoucherCodeSortParm = SortOrder == "VoucherCode" ? "VoucherCode_desc" : "VoucherCode";
                ViewBag.RedeemedStoreDateSortParm = SortOrder == "RedeemedStoreDate" ? "RedeemedStoreDate_desc" : "RedeemedStoreDate";
                ViewBag.StoreNameSortParm = SortOrder == "StoreName" ? "StoreName_desc" : "StoreName";
                ViewBag.RedeemDateSortParm = SortOrder == "RedeemDate" ? "RedeemDate_desc" : "RedeemDate";
            }
            switch (SortOrder)
            {
                case "Name":
                    CouponsLst = CouponsLst.OrderBy(s => s.Consumer.Name);
                    break;
                case "Name_desc":
                    CouponsLst = CouponsLst.OrderByDescending(s => s.Consumer.Name);
                    break;
                case "VoucherCode":
                    CouponsLst = CouponsLst.OrderBy(s => s.Voucher);
                    break;
                case "VoucherCode_desc":
                    CouponsLst = CouponsLst.OrderByDescending(s => s.Voucher);
                    break;
                case "RedeemedStoreDate":
                    CouponsLst = CouponsLst.OrderBy(s => s.RedeemStoreDate);
                    break;
                case "RedeemedStoreDate_desc":
                    CouponsLst = CouponsLst.OrderByDescending(s => s.RedeemStoreDate);
                    break;
                case "StoreName":
                    CouponsLst = CouponsLst.OrderBy(s => s.Consumer.StoreName);
                    break;
                case "StoreName_desc":
                    CouponsLst = CouponsLst.OrderByDescending(s => s.Consumer.StoreName);
                    break;
                case "RedeemDate":
                    CouponsLst = CouponsLst.OrderBy(s => s.RedeemDate);
                    break;
                case "RedeemDate_desc":
                    CouponsLst = CouponsLst.OrderByDescending(s => s.RedeemDate);
                    break;
            }

            int PageNumber = (page ?? 1);
            TempData["RedeemedSortOrder"] = SortOrder;
            TempData["RedeemedCouponsTemp"] = CouponsLst.ToList();
            return View(CouponsLst.ToPagedList(PageNumber, int.Parse(System.Configuration.ConfigurationManager.AppSettings["PageSize"])));
        }

        [CustomAuthorize(Roles = "Registered")]
        public ActionResult Voucher()
        {
            // Send confirmation mail
            //Utilities.UtilityHelper.SendMail(Winner.ConsumerModel.Email, "Danone Rewards Draw", Rwrd.Message);

            return View();
        }

        [CustomAuthorize(Roles = "Registered")]
        public ActionResult MyVouchers()
        {
            var CurrentUser = db.Consumers.FirstOrDefault(x => x.Email == User.Identity.Name.ToString());
            if (CurrentUser != null)
            {
                var Coupons = db.Coupons.Where(c => c.RedeemedBy == CurrentUser.ConsumerId).OrderByDescending(c => c.RedeemDate).ToList();
                return View(Coupons);
            }
            else
            {
                return HttpNotFound();
            }
        }

        public ActionResult CouponValide(string CouponCode, string SubmitButton)
        {
            CouponCode = DanoneRewards.Utilities.UtilityHelper.AddCouponDashes(CouponCode);
            var ObjEntity = db.Coupons.FirstOrDefault(i => i.CouponCode == CouponCode);

            if (ObjEntity == null)
            {
                TempData["CouponValidation"] = "Invalid";
                return RedirectToAction("Index", "Home");
            }
            else
            {
                if (ObjEntity.RedeemedBy == null)
                {

                    TempData["CouponCode"] = CouponCode;
                    TempData["VoucherCode"] = ObjEntity.Voucher;
                    TempData["CouponKey"] = ObjEntity.CouponId.ToString();
                    if (Request.IsAuthenticated)
                    {

                        var currentUser = db.Consumers.Where(x => x.Email == User.Identity.Name.ToString()).FirstOrDefault();
                        var CouponId = int.Parse(TempData.Peek("CouponKey").ToString());
                        var ObjCoupon = db.Coupons.Where(x => x.CouponId == CouponId).FirstOrDefault();
                        ObjCoupon.RedeemedBy = currentUser.ConsumerId;
                        ObjCoupon.RedeemDate = DateTime.Now;
                        db.Entry(ObjCoupon).State = EntityState.Modified;
                        db.SaveChanges();
                        Utilities.UtilityHelper.SendRedeemedVoucherMail(currentUser.Email, ObjCoupon.Voucher, ObjCoupon.RedeemDate.Value.ToString("dd/MM/yyyy hh:mm"));
                        return RedirectToAction("Voucher", "Coupons");
                    }
                    else
                    {
                        if (SubmitButton == DanoneRewards.Content.MyResources.Resource.Register)
                            return RedirectToAction("RegisterPage", "Consumers");
                        else
                            return RedirectToAction("LoginPage", "Consumers");
                    }
                }
                else
                {
                    TempData["CouponValidation"] = "Used";
                    return RedirectToAction("Index", "Home");
                }
            }
        }
        
        public ActionResult Validate(string CouponCode, string SubmitButton)
        {
            return CouponValide(CouponCode, SubmitButton);
        }

        //// GET: Coupons/Details/5
        //public ActionResult Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Coupon coupon = db.Coupons.Find(id);
        //    if (coupon == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(coupon);
        //}

        // GET: Coupons/Create


        //[CustomAuthorize(Roles = "AdminUser")]
        //public ActionResult Create()
        //{
        //    ViewBag.RedeemedBy = new SelectList(db.Consumers, "ConsumerId", "Name");
        //    return View();
        //}

        // POST: Coupons/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.


        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "CouponId,CouponCode,CreationDate,RedeemDate,RedeemedBy")] Coupon coupon)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Coupons.Add(coupon);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    ViewBag.RedeemedBy = new SelectList(db.Consumers, "ConsumerId", "Name", coupon.RedeemedBy);
        //    return View(coupon);
        //}




        // GET: Coupons/Edit/5
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Coupon coupon = db.Coupons.Find(id);
        //    if (coupon == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.RedeemedBy = new SelectList(db.Consumers, "ConsumerId", "Name", coupon.RedeemedBy);
        //    return View(coupon);
        //}



        // POST: Coupons/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "CouponId,CouponCode,CreationDate,RedeemDate,RedeemedBy")] Coupon coupon)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(coupon).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.RedeemedBy = new SelectList(db.Consumers, "ConsumerId", "Name", coupon.RedeemedBy);
        //    return View(coupon);
        //}



        //// GET: Coupons/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Coupon coupon = db.Coupons.Find(id);
        //    if (coupon == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(coupon);
        //}


        ////// POST: Coupons/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    Coupon coupon = db.Coupons.Find(id);
        //    db.Coupons.Remove(coupon);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        // GET: Coupons/Generate
        public ActionResult Generate()
        {
            return View();

        }

        // POST: Coupons/Generate
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [CustomAuthorize(Roles = "AdminUser")]
        public ActionResult Generate(int Number)
        {
            bool Result = GenerateCoupons(Number);
            if (Result == true)
                return new HttpStatusCodeResult(HttpStatusCode.OK);
            else
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        }

        [CustomAuthorize(Roles = "AdminUser")]
        public bool GenerateCoupons(int number)
        {
            var list = new Coupon();
            List<Coupon> newCoupons = new List<Coupon>();

            Random rnd = new Random();
            int lastIndex = db.Coupons.Count();

            for (int i = 0; i < number; i++)
            {
                string code = rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + "-" +
                                 rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + "-" +
                                    rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + "-" +
                                        rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "";

                string VoucherCode = rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + "-" +
                                 rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + "-" +
                                    rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + "-" +
                                        rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "";
                Coupon coupon = new Coupon();
                coupon.CouponCode = code;
                coupon.Voucher = VoucherCode;
                var result = db.Coupons.Add(coupon);
                Console.Write(result);
                try
                {
                    db.SaveChanges();
                    newCoupons.Add(coupon);

                }
                catch (Exception e)
                {
                    Console.Write(e);
                    i--;
                }
            }
            Utilities.UtilityHelper.ExportToExcel(newCoupons, Response, "Generated_Codes");

            //string attachment = "attachment; filename=MyCsvLol.csv";

            return true;
        }

        //public void FillVoucher()
        //{
        //    Random rnd = new Random();
        //    var lstAddedVouchers = new List<string>();
        //    var lstCoupons = db.Coupons.Where(s => s.Voucher == null).ToList();
        //    foreach (var item in lstCoupons)
        //    {
        //        //string code = rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + "-" +
        //        //                rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + "-" +
        //        //                   rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + "-" +
        //        //                       rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "";

        //        var CodeNew = GenerateVoucherCode(lstAddedVouchers);
        //        lstAddedVouchers.Add(CodeNew);
        //        item.Voucher = CodeNew;
                
        //    }
        //    try
        //    {
        //        db.SaveChanges();

        //    }
        //    catch (Exception e)
        //    {
        //        Console.Write(e);
        //    }
        //}

        //private string GenerateVoucherCode(List<string> lstAddedVouchers)
        //{
        //    var AddedNew = false;
        //    string code = "";
        //    Random rnd = new Random();
        //    while (!AddedNew)
        //    {
        //        code = rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + "-" +
        //                       rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + "-" +
        //                          rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + "-" +
        //                              rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "";
        //        if (!lstAddedVouchers.Contains(code))
        //            AddedNew = true;
        //    }
        //    return code;
        //}

        [CustomAuthorize(Roles = "AdminUser")]
        public void ExportCsv()
        {
            Consumer currentUser = db.Consumers.Where(x => x.Email == User.Identity.Name.ToString()).FirstOrDefault();
            var Coupons = db.Coupons.Where(c => c.RedeemedBy == currentUser.ConsumerId).ToList();

            Utilities.UtilityHelper.ExportToExcel(Coupons, Response, currentUser.Name + "_Vouchers");
        }

        [CustomAuthorize(Roles = "AdminUser")]
        public void ExportRedeemedCsv()
        {

            List<Coupon> tempRedeemed = (List<Coupon>)TempData.Peek("RedeemedCouponsTemp");
            Utilities.UtilityHelper.ExportToExcel(tempRedeemed.Select(c => new
            {
                CouponCode = c.CouponCode,
                RedeemDate = c.RedeemDate,
                ConsumerName = c.Consumer.Name,
                Voucher = c.Voucher,
                RedeemStoreDate = c.RedeemStoreDate,
                StoreName = c.Store == null ? "" : c.Store.Name
            }).ToList(), Response, "Danone_Redeemed_Coupons");
        }

        public ActionResult CouponLink(string data)
        {
            TempData["VoucherCode"] = data;
            return RedirectToAction("Voucher");
        }
    }
}