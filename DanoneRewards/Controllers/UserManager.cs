﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DanoneRewards.Models;
using System.Data.Entity;

namespace DanoneRewards.Controllers
{
    class UserManager
    {
        public bool IsValid(string username, string password)
        {
            using (var db = new DanoneRewardsEntities())
            {
                return db.Consumers.AsNoTracking().Any(u => u.Email == username
                    && u.Password == password);
            }
        }
        public bool AdminValid(string username, string password)
        {
            using (var db = new DanoneRewardsEntities())
            {
                return db.Users.AsNoTracking().Any(u => u.Username == username
                    && u.Password == password);
            }
        }

    }
}