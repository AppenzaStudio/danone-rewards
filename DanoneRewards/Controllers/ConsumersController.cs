﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DanoneRewards.Models;
using System.Security.Claims;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using DanoneRewards.Utilities;
using DanoneRewards.ViewModels;
using PagedList;
using DanoneRewards.Content.MyResources;
namespace DanoneRewards.Controllers
{
    public class ConsumersController : BaseController
    {
        private DanoneRewardsEntities db = new DanoneRewardsEntities();
        // GET: Consumers
        [CustomAuthorize(Roles = "AdminUser")]
        public ActionResult Index(string SortOrder, string City, string Area, string CouponNum, string currentFilter, string searchString, int? page)
        {
            var ConsumersLst = from s in db.Consumers select s;
            ConsumersLst = ConsumersLst.Include(s => s.Coupons);
            var NumberOfConsumers  = ConsumersLst.Count();
            TempData["NumberOfConsumers"] = NumberOfConsumers;
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewBag.CurrentFilter = searchString;

            City = string.IsNullOrEmpty(City) ? TempData.Peek("CityFilter") == null ? "" : TempData["CityFilter"].ToString() : City;
            if (!string.IsNullOrEmpty(City))
            {
                ConsumersLst = ConsumersLst.Where(s => s.City.Equals(City));
            }
            TempData["CityFilter"] = City;

            Area = string.IsNullOrEmpty(Area) ? TempData.Peek("AreaFilter") == null ? "" : TempData["AreaFilter"].ToString() : Area;
            if (!string.IsNullOrEmpty(Area))
            {
                ConsumersLst = ConsumersLst.Where(s => s.Area.Equals(Area));
            }
            TempData["AreaFilter"] = Area;

            CouponNum = string.IsNullOrEmpty(CouponNum) ? TempData.Peek("CouponNumFilter") == null ? "" : TempData["CouponNumFilter"].ToString() : CouponNum;
            if (!string.IsNullOrEmpty(CouponNum))
            {
                int CouponsCount = int.Parse(CouponNum);
                ConsumersLst = ConsumersLst.Where(c => c.Coupons.Count > CouponsCount);
            }
            TempData["CouponNumFilter"] = CouponNum;


            if (string.IsNullOrEmpty(SortOrder) && TempData.Peek("RegisteredSortOrder") == null)
            {
                SortOrder = "Name";
                ViewBag.NameSortParm = "Name";
                ViewBag.ChildNameSortParm = "ChildName";
                ViewBag.ChildAgeSortParm = "ChildAge";
                ViewBag.AreaSortParm = "Area";
                ViewBag.CitySortParm = "City";
                ViewBag.DoctorNameSortParm = "DoctorName";
                ViewBag.StoreNameSortParm = "StoreName";
                ViewBag.CouponNumSortParm = "CouponNum";
            }
            else if (string.IsNullOrEmpty(SortOrder) && TempData.Peek("RegisteredSortOrder") != null)
            {
                SortOrder = TempData.Peek("RegisteredSortOrder").ToString();
                ViewBag.NameSortParm = "Name";
                ViewBag.ChildNameSortParm = "ChildName";
                ViewBag.ChildAgeSortParm = "ChildAge";
                ViewBag.AreaSortParm = "Area";
                ViewBag.CitySortParm = "City";
                ViewBag.DoctorNameSortParm = "DoctorName";
                ViewBag.StoreNameSortParm = "StoreName";
                ViewBag.CouponNumSortParm = "CouponNum";
            }
            else
            {
                ViewBag.NameSortParm = SortOrder == "Name" ? "Name_desc" : "Name";
                ViewBag.ChildNameSortParm = SortOrder == "ChildName" ? "ChildName_desc" : "ChildName";
                ViewBag.ChildAgeSortParm = SortOrder == "ChildAge" ? "ChildAge_desc" : "ChildAge";
                ViewBag.AreaSortParm = SortOrder == "Area" ? "Area_desc" : "Area";
                ViewBag.CitySortParm = SortOrder == "City" ? "City_desc" : "City";
                ViewBag.DoctorNameSortParm = SortOrder == "DoctorName" ? "DoctorName_desc" : "DoctorName";
                ViewBag.StoreNameSortParm = SortOrder == "StoreName" ? "StoreName_desc" : "StoreName";
                ViewBag.CouponNumSortParm = SortOrder == "CouponNum" ? "CouponNum_desc" : "CouponNum";
            }
            switch (SortOrder)
            {
                case "Name":
                    ConsumersLst = ConsumersLst.OrderBy(s => s.Name);
                    break;
                case "Name_desc":
                    ConsumersLst = ConsumersLst.OrderByDescending(s => s.Name);
                    break;
                case "ChildName":
                    ConsumersLst = ConsumersLst.OrderBy(s => s.ChildName);
                    break;
                case "ChildName_desc":
                    ConsumersLst = ConsumersLst.OrderByDescending(s => s.ChildName);
                    break;
                case "ChildAge":
                    ConsumersLst = ConsumersLst.OrderBy(s => s.ChildAge);
                    break;
                case "ChildAge_desc":
                    ConsumersLst = ConsumersLst.OrderByDescending(s => s.ChildAge);
                    break;
                case "Area":
                    ConsumersLst = ConsumersLst.OrderBy(s => s.Area);
                    break;
                case "Area_desc":
                    ConsumersLst = ConsumersLst.OrderByDescending(s => s.Area);
                    break;
                case "City":
                    ConsumersLst = ConsumersLst.OrderBy(s => s.City);
                    break;
                case "City_desc":
                    ConsumersLst = ConsumersLst.OrderByDescending(s => s.City);
                    break;
                case "DoctorName":
                    ConsumersLst = ConsumersLst.OrderBy(s => s.DoctorName);
                    break;
                case "DoctorName_desc":
                    ConsumersLst = ConsumersLst.OrderByDescending(s => s.DoctorName);
                    break;
                case "StoreName":
                    ConsumersLst = ConsumersLst.OrderBy(s => s.StoreName);
                    break;
                case "StoreName_desc":
                    ConsumersLst = ConsumersLst.OrderByDescending(s => s.StoreName);
                    break;
                case "CouponNum":
                    ConsumersLst = ConsumersLst.OrderBy(s => s.Coupons.Count);
                    break;
                case "CouponNum_desc":
                    ConsumersLst = ConsumersLst.OrderByDescending(s => s.Coupons.Count);
                    break;
            }
            int PageNumber = (page ?? 1);
            var Result = ConsumersLst.Select(c => new RegisteredConsumers { ConsumerModel = c, CouponsNum = c.Coupons.Count }).ToPagedList(PageNumber, int.Parse(System.Configuration.ConfigurationManager.AppSettings["PageSize"]));
            var ResultList = ConsumersLst.Select(c => new RegisteredConsumers { ConsumerModel = c, CouponsNum = c.Coupons.Count }).ToList();
            TempData["RegisteredSortOrder"] = SortOrder;
            TempData["RegisteredConsumersTemp"] = ResultList;
            TempData["ConsumersTemp"] = ConsumersLst;
            return View(Result);

        }



        // GET: Consumers/Details/5
        //public ActionResult Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Consumer consumer = db.Consumers.Find(id);
        //    if (consumer == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(consumer);
        //}



        // GET: Consumers/Create
        public ActionResult Create()
        {
            return View();
        }
        
        [HttpGet]
        public ActionResult RegisterPage()
        {
            return View("RegisterPage");
        }


        public ActionResult LoginPage()
        {
            return View("LoginPage");
        }
        public void loginFunc(string username, string password, bool Ispresistent = true)
        {
            var ident = new ClaimsIdentity(
              new[] { 
              new Claim(ClaimTypes.NameIdentifier, username),
              new Claim("http://schemas.microsoft.com/accesscontrolservice/2010/07/claims/identityprovider", "ASP.NET Identity", "http://www.w3.org/2001/XMLSchema#string"),
              new Claim(ClaimTypes.Name,username),
              new Claim(ClaimTypes.Role, "Registered")

          },
              DefaultAuthenticationTypes.ApplicationCookie);

            HttpContext.GetOwinContext().Authentication.SignIn(
           new AuthenticationProperties { IsPersistent = false, ExpiresUtc = DateTime.Now.AddYears(1) }, ident);
        }
        [HttpPost]
        public ActionResult Login(ConsumerLoginRegister model)
        {
            if (TryValidateModel(model.ObjLoginViewModel))
            {
                string username = model.ObjLoginViewModel.Email;
                string password = SecurityManager.GetMD5Password(model.ObjLoginViewModel.Password);
                bool rememberMe = model.ObjLoginViewModel.RememberMe;
                if (new UserManager().IsValid(username, password))
                {
                    loginFunc(username, password, rememberMe);
                    if (TempData.Peek("CouponKey") != null && TempData.Peek("CouponKey").ToString() != "")
                    {
                        var CouponId = int.Parse(TempData.Peek("CouponKey").ToString());
                        var ObjCoupon = db.Coupons.Where(x => x.CouponId == CouponId).FirstOrDefault();
                        if (ObjCoupon.RedeemedBy != null)
                            return RedirectToAction("Index", "Home");

                        ObjCoupon.RedeemedBy = db.Consumers.AsNoTracking().FirstOrDefault(u => u.Email == username).ConsumerId;
                        ObjCoupon.RedeemDate = DateTime.Now;
                        db.Entry(ObjCoupon).State = EntityState.Modified;
                        db.SaveChanges();
                        Utilities.UtilityHelper.SendRedeemedVoucherMail(username, ObjCoupon.Voucher, ObjCoupon.RedeemDate.Value.ToString("dd/MM/yyyy hh:mm"));
                        return RedirectToAction("Voucher", "Coupons");
                    }
                    else
                    {
                        return RedirectToAction("Index", "Home");
                    }
                }
                else
                {
                    ModelState.AddModelError("", Resource.InvalidUserName);
                    TempData["LoginError"] = "LoginError";
                    return View("LoginPage");
                }
            }
            else
            {
                return View("LoginPage");
            }

        }
        [HttpPost]
        public ActionResult FacebookLogin(FacebookViewModel model)
        {
            TempData["ConsumerName"] = model.Name;
            TempData["ConsumerEmail"] = model.Email;
            TempData["ConsumerFbId"] = model.FacebookId;
            TempData["ConsumerFbkToken"] = model.FacebookToken;
            var ObjConsumer = db.Consumers.Where(x => x.FbId == model.FacebookId).FirstOrDefault();
            if (ObjConsumer != null)
            {
                loginFunc(ObjConsumer.Email, ObjConsumer.Password);
                if (TempData.Peek("CouponCode") != null && TempData.Peek("CouponCode").ToString() != "")
                    return Json("/" + RouteData.Values["culture"].ToString() + "/Coupons/Voucher");
                else
                    return Json("/" + RouteData.Values["culture"].ToString() + "/Home/Index");
            }
            return Json("/" + RouteData.Values["culture"].ToString() + "/Consumers/Create");
        }
        public ActionResult ContinueToRegestraion(ConsumerLoginRegister data)
        {
            if (TryValidateModel(data.ObjRegisterViewModel))
            {
                if (db.Consumers.AsNoTracking().Any(u => u.Email == data.ObjRegisterViewModel.Email))
                {
                    ModelState.AddModelError("ObjRegisterViewModel.Email", Resource.EmailExist);
                    return View("RegisterPage");
                }
                TempData["ConsumerName"] = data.ObjRegisterViewModel.Name;
                TempData["ConsumerEmail"] = data.ObjRegisterViewModel.Email;
                TempData["ConsumerPassword"] = data.ObjRegisterViewModel.Password;
                TempData["ConsumerMobile"] = data.ObjRegisterViewModel.Mobile;
                return RedirectToAction("Create");
            }
            else
            {
                return View("RegisterPage");
            }
        }


        // POST: Consumers/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create(Consumer Consumer)
        {
            if (TempData.Peek("ConsumerName") != null && TempData.Peek("ConsumerName").ToString() != "")
                Consumer.Name = TempData.Peek("ConsumerName").ToString();

            if (TempData.Peek("ConsumerMobile") != null && TempData.Peek("ConsumerMobile").ToString() != "")
                Consumer.MobileNumber = TempData.Peek("ConsumerMobile").ToString();

            if (TempData.Peek("ConsumerEmail") != null && TempData.Peek("ConsumerEmail").ToString() != "")
                Consumer.Email = TempData.Peek("ConsumerEmail").ToString();

            if (TempData.Peek("ConsumerPassword") != null && TempData.Peek("ConsumerPassword").ToString() != "")
                Consumer.Password = TempData.Peek("ConsumerPassword").ToString();

            if (TempData.Peek("ConsumerFbId") != null && TempData.Peek("ConsumerFbId").ToString() != "")
                Consumer.FbId = TempData.Peek("ConsumerFbId").ToString();

            if (TempData.Peek("ConsumerFbkToken") != null && TempData.Peek("ConsumerFbkToken").ToString() != "")
                Consumer.FbToken = TempData.Peek("ConsumerFbkToken").ToString();
            var CounsumerFacebookId = TempData.Peek("ConsumerFbId");
            ModelState.Clear();
            if (TryValidateModel(Consumer))
            {
                if (CounsumerFacebookId == null)
                {
                    Consumer.Password = SecurityManager.GetMD5Password(Consumer.Password);
                }
                db.Consumers.Add(Consumer);
                db.SaveChanges();
                loginFunc(Consumer.Email, Consumer.Password);

                if (TempData.Peek("CouponKey") != null && TempData.Peek("CouponKey").ToString() != "")
                {
                    var CouponId = int.Parse(TempData.Peek("CouponKey").ToString());
                    var ObjCoupon = db.Coupons.Where(x => x.CouponId == CouponId).FirstOrDefault();
                    ObjCoupon.RedeemedBy = Consumer.ConsumerId;
                    ObjCoupon.RedeemDate = DateTime.Now;
                    db.Entry(ObjCoupon).State = EntityState.Modified;
                    db.SaveChanges();
                    Utilities.UtilityHelper.SendRedeemedVoucherMail(Consumer.Email, ObjCoupon.Voucher, ObjCoupon.RedeemDate.Value.ToString("dd/MM/yyyy hh:mm"));
                    return RedirectToAction("Voucher", "Coupons");
                }
                else
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                var errors = ModelState.Select(x => x.Value.Errors)
                                       .Where(y => y.Count > 0)
                                       .ToList();
            }
            return View(Consumer);
        }

        //// GET: Consumers/Edit/5
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Consumer consumer = db.Consumers.Find(id);
        //    if (consumer == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(consumer);
        //}

        // POST: Consumers/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "ConsumerId,Name,ChildName,ChildAge,Area,City,DoctorName,DoctorArea,DoctorCity,DoctorAddress,StoreName,FbId,FbToken,Email,Password,Gender,Birthdate")] Consumer consumer)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(consumer).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    return View(consumer);
        //}

        //// GET: Consumers/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Consumer consumer = db.Consumers.Find(id);
        //    if (consumer == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(consumer);
        //}

        //// POST: Consumers/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    Consumer consumer = db.Consumers.Find(id);
        //    db.Consumers.Remove(consumer);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult ResetPassword(string id)
        {
            if (id != null)
            {
                ConsumerForgetPassword obj = db.ConsumerForgetPasswords.FirstOrDefault(x => x.Code == id);

                if (obj == null)
                {
                    // no such code
                    TempData["ResetCodeValidation"] = "404";
                    return View();
                }
                else if (obj.ExpiryDate < DateTime.Now)
                {
                    // code expired
                    TempData["ResetCodeValidation"] = "Expired";
                    return View();
                }
                else
                {
                    // code valid redirect to new password page
                    TempData["ResetCodeValidation"] = "Valid";
                    TempData["ResetUserId"] = obj.ConsumerId;
                    return View();
                }
            }
            else
            {
                return null;
            }
        }

        public ActionResult ResetPasswordValidation(string NewPassword, string NewPasswordConfirmation)
        {
            if (!string.IsNullOrEmpty(NewPassword) && NewPassword == NewPasswordConfirmation)
            {
                if (TempData.Peek("ResetUserId") != null)
                {
                    int consumerId = int.Parse(TempData.Peek("ResetUserId").ToString());
                    Consumer Cons = db.Consumers.Find(consumerId);
                    Cons.Password = SecurityManager.GetMD5Password(NewPassword);
                    db.Entry(Cons).State = EntityState.Modified;
                    db.SaveChanges();

                    var ObjConsumerForget = db.ConsumerForgetPasswords.Where(x => x.ConsumerId == consumerId).ToList();
                    foreach (var item in ObjConsumerForget)
                    {
                        item.ExpiryDate = DateTime.Now;
                        db.Entry(item).State = EntityState.Modified;
                    }
                    db.SaveChanges();
                    loginFunc(Cons.Email, Cons.Password, true);

                    return RedirectToAction("Index", "Home");
                }
            }
            else
            {
                return View();
            }
            return View();
        }

        [CustomAuthorize(Roles = "AdminUser")]
        public void ExportCsv()
        {

            List<RegisteredConsumers> result = (List<RegisteredConsumers>)TempData.Peek("RegisteredConsumersTemp");
            var exportList = result.Select(c => new
            {
                c.ConsumerModel.Name,
                c.ConsumerModel.Email,
                c.ConsumerModel.MobileNumber,
                c.ConsumerModel.City,
                c.ConsumerModel.Area,
                c.ConsumerModel.ChildName,
                c.ConsumerModel.ChildAge,
                c.ConsumerModel.DoctorName,
                c.ConsumerModel.DoctorCity,
                c.ConsumerModel.DoctorArea,
                c.ConsumerModel.DoctorAddress,
                c.ConsumerModel.StoreName,
                c.CouponsNum
            }).ToList();

            //Consumer currentUser = db.Consumers.Where(x => x.Email == User.Identity.Name.ToString()).FirstOrDefault();
            Utilities.UtilityHelper.ExportToExcel(exportList, Response, "Danone_Consumers");
            //TempData.Remove("RegisteredConsumersTemp");
        }
        public ActionResult AutoCompleteDoctorName(string query)
        {
            var Lst = db.Consumers.Where(c => c.DoctorName.Contains(query)).Select(c => c.DoctorName).Take(5).ToArray();
            return Json(new { query = query, suggestions = Lst }, JsonRequestBehavior.AllowGet);
        }
        public ActionResult AutoCompleteStoreName(string query)
        {
            var Lst = db.Consumers.Where(c => c.StoreName.Contains(query)).Select(c => c.StoreName).Take(5).ToArray();
            return Json(new { query = query, suggestions = Lst }, JsonRequestBehavior.AllowGet);
        }
    }
}