﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DanoneRewards.Models;
using DanoneRewards.ViewModels;
using PagedList;

namespace DanoneRewards.Controllers
{
    public class RewardsController : BaseController
    {
        private DanoneRewardsEntities db = new DanoneRewardsEntities();

        // GET: Rewards
        [CustomAuthorize(Roles = "AdminUser")]
        public ActionResult Index(string SortOrder, string currentFilter, string searchString, string Name, int? page)
        {
            var rewardsLst = from s in db.Rewards select s;
            var rewards = db.Rewards.Include(r => r.Consumer);
            var NumberOfRewards = rewardsLst.Count();
            TempData["NumberOfRewards"] = NumberOfRewards;
            if (searchString != null)
            {
                page = 1;
            }
            else
            {
                searchString = currentFilter;
            }

            if (string.IsNullOrEmpty(Name))
            {
                Name = "";
            }
            else
            {
                Name = string.IsNullOrEmpty(Name) ? TempData["NameFilter"] == null ? "" : TempData["NameFilter"].ToString() : Name;
                rewards = rewards.Where(s => s.Consumer.Name.Contains(Name));
            }

            TempData["NameFilter"] = Name;
            ViewBag.CurrentFilter = searchString;
            if (string.IsNullOrEmpty(SortOrder) && TempData.Peek("RewardSortOrder") == null)
            {
                SortOrder = "Name";
                ViewBag.NameSortParm = "Name";
                ViewBag.MessageSortParm = "Message";
                ViewBag.GenerationDateSortParm = "GenerationDate";
            }
            else if (string.IsNullOrEmpty(SortOrder) && TempData.Peek("RewardSortOrder") != null)
            {
                SortOrder = TempData.Peek("RewardSortOrder").ToString();
                ViewBag.NameSortParm = "Name";
                ViewBag.MessageSortParm = "Message";
                ViewBag.GenerationDateSortParm = "GenerationDate";
            }
            else
            {
                ViewBag.NameSortParm = SortOrder == "Name" ? "Name_desc" : "Name";
                ViewBag.MessageSortParm = SortOrder == "Message" ? "Message_desc" : "Message";
                ViewBag.GenerationDateSortParm = SortOrder == "GenerationDate" ? "GenerationDate_desc" : "GenerationDate";
            }

            switch (SortOrder)
            {
                case "Name":
                    rewards = rewards.OrderBy(r => r.Consumer.Name);
                    break;
                case "Name_desc":
                    rewards = rewards.OrderByDescending(r => r.Consumer.Name);
                    break;
                case "Message":
                    rewards = rewards.OrderBy(r => r.Message);
                    break;
                case "Message_desc":
                    rewards = rewards.OrderByDescending(r => r.Message);
                    break;
                case "GenerationDate":
                    rewards = rewards.OrderBy(r => r.GenerationDate);
                    break;
                case "GenerationDate_desc":
                    rewards = rewards.OrderByDescending(r => r.GenerationDate);
                    break;
            }
            int PageNumber = (page ?? 1);
            TempData["RewardSortOrder"] = SortOrder;
            TempData["RewardsTemp"] = rewards.ToList();
            return View(rewards.ToPagedList(PageNumber, int.Parse(System.Configuration.ConfigurationManager.AppSettings["PageSize"])));
        }
        // GET: Rewards/Details/5
        //public ActionResult Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Reward reward = db.Rewards.Find(id);
        //    if (reward == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(reward);
        //}

        // GET: Rewards/Create
        //public ActionResult Create()
        //{
        //    ViewBag.ConsumerId = new SelectList(db.Consumers, "ConsumerId", "Name");
        //    return View();
        //}

        // POST: Rewards/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create([Bind(Include = "RewardId,ConsumerId,Message,GenerationDate")] Reward reward)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Rewards.Add(reward);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    ViewBag.ConsumerId = new SelectList(db.Consumers, "ConsumerId", "Name", reward.ConsumerId);
        //    return View(reward);
        //}

        // GET: Rewards/Edit/5
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Reward reward = db.Rewards.Find(id);
        //    if (reward == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.ConsumerId = new SelectList(db.Consumers, "ConsumerId", "Name", reward.ConsumerId);
        //    return View(reward);
        //}

        // POST: Rewards/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "RewardId,ConsumerId,Message,GenerationDate")] Reward reward)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(reward).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.ConsumerId = new SelectList(db.Consumers, "ConsumerId", "Name", reward.ConsumerId);
        //    return View(reward);
        //}

        // GET: Rewards/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Reward reward = db.Rewards.Find(id);
        //    if (reward == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(reward);
        //}

        // POST: Rewards/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    Reward reward = db.Rewards.Find(id);
        //    db.Rewards.Remove(reward);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [CustomAuthorize(Roles = "AdminUser")]
        public void ExportCsv()
        {
            Consumer currentUser = db.Consumers.Where(x => x.Email == User.Identity.Name.ToString()).FirstOrDefault();
            var Rewards = db.Rewards.Select(r => new { r.Consumer.Name, r.Message, r.GenerationDate }).ToList();
            Utilities.UtilityHelper.ExportToExcel(Rewards, Response, "Danone_Rewards_List");
        }

        [CustomAuthorize(Roles = "AdminUser")]
        public void ExportRwardsCsv()
        {

            List<Reward> tempRwards= (List<Reward>)TempData.Peek("RewardsTemp");
            Utilities.UtilityHelper.ExportToExcel(tempRwards.Select(c => new
            {
                c.Consumer.Name,
                c.Message,
                c.GenerationDate        
            }).ToList(), Response, "Danone_Reward_Coupons");
        }

        [CustomAuthorize(Roles = "AdminUser")]
        public ActionResult DrawReward()
        {

            List<RegisteredConsumers> listCons = (List<RegisteredConsumers>)TempData.Peek("RegisteredConsumersTemp");
            var qry = from row in listCons
                      select row;
            int count = listCons.Count();
            int IndexCons = new Random().Next(count);
            var result = qry.Where((row, index) => index == IndexCons);
            RegisteredConsumers Winner = result.FirstOrDefault();
            TempData["CurrentDrawWinner"] = Winner;
            TempData["CurrentDrawWinnerName"] = Winner.ConsumerModel.Name;
            return RedirectToAction("ContinueDraw");
        }


        [CustomAuthorize(Roles = "AdminUser")]
        public ActionResult ContinueDraw()
        {
            return View();
        }
        [CustomAuthorize(Roles = "AdminUser")]
        public ActionResult ConfirmDraw(string winnerMsg)
        {
            RegisteredConsumers Winner = (RegisteredConsumers)TempData.Peek("CurrentDrawWinner");
            Reward Rwrd = new Reward();
            Rwrd.GenerationDate = DateTime.Now;
            Rwrd.Message = winnerMsg;
            Rwrd.ConsumerId = Winner.ConsumerModel.ConsumerId;
            db.Rewards.Add(Rwrd);
            db.SaveChanges();
            Utilities.UtilityHelper.SendMail(Winner.ConsumerModel.Email, "Danone Rewards Draw", Rwrd.Message);
            return RedirectToAction("Index", "Consumers");
        }


    }
}