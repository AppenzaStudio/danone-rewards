﻿using DanoneRewards.Models;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.Linq;
using System.Collections.Generic;
using DanoneRewards.Utilities;
using DanoneRewards.Content.MyResources;
using System;
namespace DanoneRewards.Controllers
{
    public class StoresController : Controller
    {
        private DanoneRewardsEntities db = new DanoneRewardsEntities();
        public class StoresMetaViewModel
        {
            [StringLength(19, ErrorMessage = "Enter a valid Voucher Code")]
            [Required(ErrorMessage = "Voucher code is required")]
            public string VoucherCodeField { set; get; }

            [Required(ErrorMessage = "Store code is required")]
            public string StoreCodeField { set; get; }
        }

        [HttpGet]
        public ActionResult SaveStore()
        {
            return View("SaveStore");
        }
        public ActionResult PrintStore()
        {

            return View("PrintStore");

        }

        [HttpPost]
        public ActionResult SaveStore(StoresMetaViewModel model)
        {
            if (ModelState.IsValid)
            {
                var CouponVoucher = model.VoucherCodeField.ToString().Trim();
                var StoreCode = model.StoreCodeField.ToString().Trim();
                long StoreId = 0;
                CouponVoucher = DanoneRewards.Utilities.UtilityHelper.AddCouponDashes(CouponVoucher);
                var StoreObj = db.Stores.AsNoTracking().FirstOrDefault(s => s.Code == StoreCode);
                var CouponObj = db.Coupons.Where(c => c.Voucher == CouponVoucher).FirstOrDefault();
                if (CouponObj != null)
                {
                    var CouponObjRedeemed = CouponObj.RedeemStoreId;
                    if (CouponObjRedeemed != null)
                    {
                        ModelState.AddModelError("", Resource.VoucherUsed);
                        return View();
                    }
                }
                if (StoreObj != null)
                {
                    StoreId = StoreObj.StoreId;
                }
                if (CouponObj != null && StoreObj != null)
                {
                    CouponObj.RedeemStoreId = StoreId;
                    CouponObj.RedeemStoreDate = DateTime.Now;
                    try
                    {
                        db.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        Console.Write(e);
                    }
                    TempData["Refresh"] = "Refresh";
                    TempData["voucherCode"] = CouponVoucher;
                    TempData["storeCode"] = StoreCode;
                    UtilityHelper.SendStoreRedeemedVoucherMail(StoreObj.Name, CouponVoucher, DateTime.Now.ToString("dd/MM/yyyy hh:mm a"));
                    return RedirectToAction("PrintStore");
                }
                else if (CouponObj == null && StoreObj != null)
                {
                    ModelState.AddModelError("", Resource.VoucherNotExist);
                }
                else if (CouponObj != null && StoreObj == null)
                {
                    ModelState.AddModelError("", Resource.StoreNotExist);
                }
                else
                {
                    ModelState.AddModelError("", Resource.StoreAndVoucherNotExist);
                }

                return View();
            }
            else
            {
                return View();
            }

        }

        //public void FillStore()
        //{
        //    Random rnd = new Random();
        //    var lstAddedCodes = new List<string>();
        //    var lstStores = db.Stores.Where(s => s.Code == null).ToList();
        //    if (lstStores != null && lstStores.Count > 0)
        //    {
        //        var CodeNew = "";
        //        foreach (var item in lstStores)
        //        {
        //            CodeNew = GenerateStoreCode(lstAddedCodes);
        //            lstAddedCodes.Add(CodeNew);
        //            item.Code = CodeNew;
        //        }
        //        try
        //        {
        //            db.SaveChanges();
        //        }
        //        catch (Exception e)
        //        {
        //            Console.Write(e);
        //        }
        //    }
        //}

        //private string GenerateStoreCode(List<string> lstAddedCodes)
        //{
        //    var AddedNew = false;
        //    string code = "";
        //    Random rnd = new Random();
        //    while (!AddedNew)
        //    {
        //        code = rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + rnd.Next(1, 10) + "" + rnd.Next(1, 10);
        //        if (!lstAddedCodes.Contains(code))
        //            AddedNew = true;
        //    }
        //    return code;
        //}
    }
}