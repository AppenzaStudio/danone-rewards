﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DanoneRewards.Controllers
{
    public class GenericController : BaseController
    {
        // GET: Generic
        public ActionResult SetCulture(string culture)
        {
            // Validate input
            culture = CultureHelper.GetImplementedCulture(culture);
            RouteData.Values["culture"] = culture;
            return RedirectToAction("Index", "Home");
        }

        public ActionResult AboutUs()
        {
            return View("AboutUs");
        }

        public ActionResult HowItWorks()
        {
            return View("HowItWorks");
        }

        public ActionResult Error()
        {
            return View("Error");
        }
    }
}