﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using DanoneRewards.Models;
using System.Net.Mail;
using System.Configuration;

namespace DanoneRewards.Controllers
{
    public class ConsumerForgetPasswordsController : BaseController
    {
        private DanoneRewardsEntities db = new DanoneRewardsEntities();

        // GET: ConsumerForgetPasswords
        public ActionResult Index()
        {
            var consumerForgetPasswords = db.ConsumerForgetPasswords.Include(c => c.Consumer);
            return View(consumerForgetPasswords.ToList());
        }

         //GET: ConsumerForgetPasswords/Create
        public ActionResult Create()
        {
            ViewBag.ConsumerId = new SelectList(db.Consumers, "ConsumerId", "Name");
            return View();
        }

         //POST: ConsumerForgetPasswords/Create
         //To protect from overposting attacks, please enable the specific properties you want to bind to, for 
         //more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ForgetPasswordId,ConsumerId,GenerationDate,ExpiryDate,Code")] ConsumerForgetPassword consumerForgetPassword)
        {
            if (ModelState.IsValid)
            {
                db.ConsumerForgetPasswords.Add(consumerForgetPassword);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.ConsumerId = new SelectList(db.Consumers, "ConsumerId", "Name", consumerForgetPassword.ConsumerId);
            return View(consumerForgetPassword);
        }

        // GET: ConsumerForgetPasswords/Details/5
        //public ActionResult Details(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    ConsumerForgetPassword consumerForgetPassword = db.ConsumerForgetPasswords.Find(id);
        //    if (consumerForgetPassword == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(consumerForgetPassword);
        //}

        // GET: ConsumerForgetPasswords/Edit/5
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    ConsumerForgetPassword consumerForgetPassword = db.ConsumerForgetPasswords.Find(id);
        //    if (consumerForgetPassword == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.ConsumerId = new SelectList(db.Consumers, "ConsumerId", "Name", consumerForgetPassword.ConsumerId);
        //    return View(consumerForgetPassword);
        //}

        // POST: ConsumerForgetPasswords/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "ForgetPasswordId,ConsumerId,GenerationDate,ExpiryDate,Code")] ConsumerForgetPassword consumerForgetPassword)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(consumerForgetPassword).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.ConsumerId = new SelectList(db.Consumers, "ConsumerId", "Name", consumerForgetPassword.ConsumerId);
        //    return View(consumerForgetPassword);
        //}

        //// GET: ConsumerForgetPasswords/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    ConsumerForgetPassword consumerForgetPassword = db.ConsumerForgetPasswords.Find(id);
        //    if (consumerForgetPassword == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(consumerForgetPassword);
        //}

        // POST: ConsumerForgetPasswords/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    ConsumerForgetPassword consumerForgetPassword = db.ConsumerForgetPasswords.Find(id);
        //    db.ConsumerForgetPasswords.Remove(consumerForgetPassword);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }


        public ActionResult ForgetPassword(ForgotPasswordViewModel ForgetPasswordModel)
        {
            Consumer obj = db.Consumers.FirstOrDefault(x => x.Email == ForgetPasswordModel.Email);
            //Check if mail already exists
            if (obj != null)
            {
                ConsumerForgetPassword ObjPasswordLink = new ConsumerForgetPassword();
                ObjPasswordLink.ConsumerId = obj.ConsumerId;
                ObjPasswordLink.GenerationDate = DateTime.Now;
                ObjPasswordLink.ExpiryDate = DateTime.Now.AddDays(1);
                ObjPasswordLink.Code = Guid.NewGuid().ToString();

                db.ConsumerForgetPasswords.Add(ObjPasswordLink);
                db.SaveChanges();

                SmtpClient SmtpServer = new SmtpClient(ConfigurationManager.AppSettings["DanoneSmtpServer"], int.Parse(ConfigurationManager.AppSettings["DanoneSmtpPort"]));
                SmtpServer.UseDefaultCredentials = false;
                SmtpServer.DeliveryMethod = SmtpDeliveryMethod.Network;
                SmtpServer.EnableSsl = true;
                SmtpServer.Credentials = new NetworkCredential(ConfigurationManager.AppSettings["DanoneMail"], ConfigurationManager.AppSettings["DanonePassword"]);

                MailMessage Mail = new MailMessage();
                Mail.From = new MailAddress(ConfigurationManager.AppSettings["DanoneSmtpFrom"]);
                Mail.To.Add(ForgetPasswordModel.Email);
                Mail.Subject = ConfigurationManager.AppSettings["DanoneSmtpSubject"];
                Mail.Body = "Dear " + obj.Name + " You have requested to reset your password , to do so please open the following link <a href="+ ConfigurationManager.AppSettings["DanoneResetPasswordLink"] + ObjPasswordLink.Code + ">Reset Password</a>";
                Mail.IsBodyHtml = true;
                SmtpServer.Send(Mail);
                TempData["CheckMail"] = "CheckMail";
            }
            return RedirectToAction("Create", "ConsumerForgetPasswords");
        }
    }
}