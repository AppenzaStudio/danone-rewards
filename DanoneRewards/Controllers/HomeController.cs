﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DanoneRewards.Controllers
{
    public class HomeController : BaseController
    {
        public ActionResult Index()
        {
            if (Request.IsAuthenticated && User.IsInRole("AdminUser"))
                return RedirectToAction("Index", "Consumers");
            else
                return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }


    }
}