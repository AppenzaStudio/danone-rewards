﻿function statusChangeCallback(response) {
    if (response.status === 'connected') {
        connectWithFacebook();
    } else if (response.status === 'not_authorized') {
        document.getElementById('status').innerHTML = 'Please log ' +
            'into this app.';
    } else {
        document.getElementById('status').innerHTML = 'Please log ' +
            'into Facebook.';
    }
}

var accessToken = 0;
function checkLoginState() {
    FB.getLoginStatus(function (response) {
        accessToken = response.authResponse.accessToken;
        statusChangeCallback(response);
    });
}
window.fbAsyncInit = function () {
    FB.init({
        appId: '1094834077229083',
        cookie: true,
        xfbml: true,
        version: 'v2.5'
    });
};
(function (d, s, id) {
    var js, fjs = d.getElementsByTagName(s)[0];
    if (d.getElementById(id)) return;
    js = d.createElement(s); js.id = id;
    js.src = "//connect.facebook.net/en_US/sdk.js";
    fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));
function connectWithFacebook() {
    FB.api('/me', 'GET', { "fields": "id,name,email,gender,birthday" }, function (response) {
        //document.getElementById('status').innerHTML = 'Thanks for logging in, ' + response.name + '!';
        $.ajax({
            type: "POST",
            contentType: "application/json; charset=utf-8",
            url: "/" + lang + "/Consumers/FacebookLogin",
            data: JSON.stringify({ 'FacebookId': response.id, 'FacebookToken': accessToken, 'Email': response.email, 'Name': response.name }),
            dataType: "json",
            success: function (data) {
                window.location.href = data;
            },
            error: function (error) {
                //alert('error; ' + eval(error));
            },
        });
    });
    //var message = 'Reading JS SDK documentation';
    //FB.api('/me/feed', 'post', { message: body}, function (response) {
    //    console.log(response.error);
    //    if (!response || response.error) {
    //        alert('Error occured');
    //    } else {
    //        alert('Post ID: ' + response.id);
    //    }
    //});
}

function FBLogin() {
    FB.login(function (response) {
        statusChangeCallback(response);
    }, { scope: 'public_profile,email,publish_actions', return_scopes: true });
}