﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using DanoneRewards.Content.MyResources;

namespace DanoneRewards.Models
{
    public class ConsumerMetadata
    {
        [Required]
        public int ConsumerId;

        [RegularExpression(
            @"([\S]+\s+){1}[\S\s]+",
            ErrorMessageResourceType = typeof(Resource),
            ErrorMessageResourceName = "NameValidationMessage"
        )]    
        [Required]
        public string Name;

        [Required(ErrorMessageResourceName = "ChildNameValidationMessage", ErrorMessageResourceType = typeof(Resource))]
        public string ChildName;

        [Required(ErrorMessageResourceName = "ChildAgeValidationMessage", ErrorMessageResourceType = typeof(Resource))]
        [Range(1, 48, ErrorMessageResourceName = "ChildAgeRangeMessage", ErrorMessageResourceType = typeof(Resource))]
        //[RegularExpression(@"^[1-4][0-8]*$")]
        public int ChildAge;
        
        [Required]
        public string Area;
        
        [Required]
        public string City;

        [Required(ErrorMessageResourceName = "DoctorNameValidationMessage", ErrorMessageResourceType = typeof(Resource))]
        public string DoctorName;
        
        [Required]
        public string DoctorArea;
        
        [Required]
        public string DoctorCity;

        [Required(ErrorMessageResourceName = "DoctorAddressValidationMessage", ErrorMessageResourceType = typeof(Resource))]
        public string DoctorAddress;

        [Required(ErrorMessageResourceName = "StoreNameValidationMessage", ErrorMessageResourceType = typeof(Resource))]
        public string StoreName;

        [Required(ErrorMessageResourceName = "EmailValidationMessage", ErrorMessageResourceType = typeof(Resource))]
        [EmailAddress]
        public string Email;
        
        public string Password;

        [Required(ErrorMessageResourceName = "PhoneNumberValidationMessage", ErrorMessageResourceType = typeof(Resource))]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(
            @"^01[0-2][0-9]{8}$",
            ErrorMessageResourceType = typeof(Resource),
            ErrorMessageResourceName = "PhoneNumberValidationMessage"
        )]
        public string MobileNumber;

    }
}