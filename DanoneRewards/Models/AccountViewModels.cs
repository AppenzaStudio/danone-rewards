﻿using System.Collections.Generic;
using DanoneRewards.Content.MyResources;
using System.ComponentModel.DataAnnotations;

namespace DanoneRewards.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ConsumerLoginRegister
    {
        public LoginViewModel ObjLoginViewModel { get; set; }
        public ConsumerViewModel ObjRegisterViewModel { get; set; }
    }

    public class AdminLoginViewModel
    {
        [Required(ErrorMessageResourceName = "EmailRequiredMessage", ErrorMessageResourceType = typeof(Resource))]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = null, ErrorMessageResourceName = "EmailValidationMessage", ErrorMessageResourceType = typeof(Resource))]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }
    public class LoginViewModel
    {
        [Required(ErrorMessageResourceName = "EmailRequiredMessage", ErrorMessageResourceType = typeof(Resource))]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = null, ErrorMessageResourceName = "EmailValidationMessage", ErrorMessageResourceType = typeof(Resource))]
        public string Email { get; set; }

        [Required(ErrorMessageResourceName = "PasswordRequiredMessage", ErrorMessageResourceType = typeof(Resource))]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Remember me?")]
        public bool RememberMe { get; set; }
    }
    public class ConsumerViewModel
    {
        [Required(ErrorMessageResourceName = "EmailRequiredMessage", ErrorMessageResourceType = typeof(Resource))]
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = null, ErrorMessageResourceName = "EmailValidationMessage", ErrorMessageResourceType = typeof(Resource))]
        public string Email { get; set; }

        [Required(ErrorMessageResourceName = "PasswordValidationMessage", ErrorMessageResourceType = typeof(Resource))]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [RegularExpression(
            @"([\S]+\s+){1}[\S\s]+",
            ErrorMessageResourceType = typeof(Resource),
            ErrorMessageResourceName = "NameValidationMessage"
        )]
        [Required(ErrorMessageResourceName = "NameValidationMessage", ErrorMessageResourceType = typeof(Resource))]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required(ErrorMessageResourceName = "PhoneNumberValidationMessage", ErrorMessageResourceType = typeof(Resource))]
        [DataType(DataType.PhoneNumber)]
        [RegularExpression(
            @"^01[0-2][0-9]{8}$",
            ErrorMessageResourceType = typeof(Resource),
            ErrorMessageResourceName = "PhoneNumberValidationMessage"
        )]
        [Display(Name = "Mobile")]
        public string Mobile { get; set; }
    }

    public class FacebookViewModel
    {
        [Display(Name = "Email")]
        [EmailAddress(ErrorMessage = null, ErrorMessageResourceName = "EmailValidationMessage", ErrorMessageResourceType = typeof(Resource))]
        public string Email { get; set; }

        [Required]
        [Display(Name = "Name")]
        public string Name { get; set; }

        [Required]
        [Display(Name = "FacebookId")]
        public string FacebookId { get; set; }

        [Required]
        [Display(Name = "FacebookToken")]
        public string FacebookToken { get; set; }
    }
    
    public class RegisterViewModel
    {
        [Required(ErrorMessage = null, ErrorMessageResourceName = "EmailRequiredMessage", ErrorMessageResourceType = typeof(Resource))]
        [EmailAddress(ErrorMessage = null, ErrorMessageResourceName = "EmailValidationMessage", ErrorMessageResourceType = typeof(Resource))]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }
    }

    public class ResetPasswordViewModel
    {
        [Required(ErrorMessage = null, ErrorMessageResourceName = "EmailRequiredMessage", ErrorMessageResourceType = typeof(Resource))]
        [EmailAddress(ErrorMessage = null, ErrorMessageResourceName = "EmailValidationMessage", ErrorMessageResourceType = typeof(Resource))]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required(ErrorMessage = null, ErrorMessageResourceName = "PasswordRequiredMessage", ErrorMessageResourceType = typeof(Resource))]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required(ErrorMessage = null, ErrorMessageResourceName = "EmailRequiredMessage", ErrorMessageResourceType = typeof(Resource))]
        [EmailAddress(ErrorMessage = null, ErrorMessageResourceName = "EmailValidationMessage", ErrorMessageResourceType = typeof(Resource))]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }
}
